use anyhow::Result;
use std::sync::Arc;
use std::time::{SystemTime, UNIX_EPOCH};
use sqlx::{FromRow, Pool, Sqlite};

pub(crate) async fn process_events(pool: Arc<Pool<Sqlite>>) -> Result<()> {

    tracing::info!("role_create initiated:");

    #[derive(FromRow, Debug)]
    struct RoleInfo {
        id: String,
        info: Option<String>,
        added_by: Option<String>,
        date_updated: String // This must be a String because sqlite does not have a u64 type and I cannot cast a u64 to i64
    }

    let data = RoleInfo {
        id: "Some ID".to_string(),
        info: None,
        added_by: None,
        date_updated: SystemTime::now().duration_since(UNIX_EPOCH).unwrap().as_secs().to_string(),
    };

    dbg!(&data.id.to_string());
    dbg!(&data.info);
    dbg!(&data.added_by);
    dbg!(&data.date_updated);

    let mut tx = pool.clone().as_ref().begin().await?;

    dbg!(&tx);

    let foobar = sqlx::query("INSERT INTO role_info (id, date_updated) VALUES (?, ?)")
        .bind(data.id.to_string())
        .bind(data.date_updated)
        .execute(&mut *tx)
        .await?;

    dbg!(&foobar);

    tx.commit().await?;

    println!("We made it to the end! Check if the data was inserted!");

    Ok(())

}