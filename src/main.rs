mod process_events;
use anyhow::Result;
use std::sync::Arc;
use std::time::Duration;
use sqlx::{Sqlite, SqlitePool};
use sqlx::migrate::MigrateDatabase;
use tokio::time::sleep;

const DATABASE_URL: &str = "sqlite://aaohelper.db";

#[tokio::main]
async fn main() -> Result<()> {

    if !Sqlite::database_exists(DATABASE_URL).await.unwrap_or(false) {
        match Sqlite::create_database(DATABASE_URL).await {
            Ok(_) => tracing::info!("Database created"),
            Err(e) => tracing::error!("Error creating database: {}", e),
        }
    } else {
        tracing::info!("Database already exists");
    }

    let pool = Arc::new(SqlitePool::connect(&DATABASE_URL)
        .await
        .unwrap());

    let result = sqlx::query("CREATE TABLE IF NOT EXISTS role_info (
        id TEXT,
        info TEXT,
        added_by TEXT,
        date_updated TEXT)"
    ).execute(pool.clone().as_ref()).await;

    match result {
        Ok(_) => tracing::info!("database created or already exists"),
        Err(e) => println!("Error creating table: {}", e)
    }

    loop {
        tokio::spawn(process_events::process_events(pool.clone()));
        sleep(Duration::from_secs(2)).await;
    }

    Ok(())

}